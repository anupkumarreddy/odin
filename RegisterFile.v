`ifndef REGISTER_FILE_COMPILED
`define REGISTER_FILE_COMPILED

module RegisterFile( clk            , //!< Clk input to the memory
                     resetn         , //!< Reset input to the memory, active low
                     readEnable1    , //!< Enables read port of the memory
                     readEnable2    , //!< Enables read port of the memory
                     readAddress1   , //!< Carries read address for read port
                     readAddress2   , //!< Carries read address for read port
                     readData1      , //!< Carries read data for read port
                     readData2      , //!< Carries read data for read port
                     writeEnable    , //!< Enables write port of the memory
                     writeAddress   , //!< Carries write address for write port
                     writeData     ); //!< Carries write data for the write port 

  // Parameters for Generic memory
  parameter MEM_AWIDTH = 32  ;
  parameter MEM_DWIDTH = 32  ;
  parameter MEM_DEPTH  = 1024;


  // Port declarations for the memory
  input  wire                  readEnable1   ;
  input  wire [MEM_AWIDTH-1:0] readAddress1  ;
  input  wire                  readEnable2   ;
  input  wire [MEM_AWIDTH-1:0] readAddress2  ;
  input  wire                  writeEnable   ;
  input  wire [MEM_AWIDTH-1:0] writeAddress  ;
  input  wire [MEM_DWIDTH-1:0] writeData     ;
  output wire [MEM_DWIDTH-1:0] readData      ;

  // Two dimentional array for the Memory
  reg [MEM_DWIDTH-1:0] memory [MEM_DEPTH];

  // Temporary signal and reg declarations
  reg [MEM_DWIDTH-1:0] readDataReg1;
  reg [MEM_DWIDTH-1:0] readDataReg2;


  // Continous Assignments
  assign readData1 = readDataReg1; 
  assign readData2 = readDataReg2; 


  // Read port for the memory
  always@( posedge clk or negedge resetn ) begin
     if( ~resetn ) begin
        readDataReg1 <= 'b0;
     end else begin
       if( readEnable1 ) begin
          readDataReg1 <= memory[readAddress1];  
       end else begin
          readDataReg1 <= 'b0;
       end
     end
  end

  // Read port for the memory
  always@( posedge clk or negedge resetn ) begin
     if( ~resetn ) begin
        readDataReg2 <= 'b0;
     end else begin
       if( readEnable2 ) begin
          readDataReg2 <= memory[readAddress2];  
       end else begin
          readDataReg2 <= 'b0;
       end
     end
  end

  // Write port for the memory
  always@( posedge clk or negedge resetn ) begin
     if( writeEnable ) begin
        memory[writeAddress] <= writeData;
     end
  end


endmodule 

`endif
