
`ifndef ALU_COMPILE
`define ALU_COMPILE

module ALU ( opcode   , //!< opcode for the alu
             operand1 , //!< first operand for the alu
             operand2 , //!< second operand for the alu
             result  ); //!< result of the alu 

  // parameters declaration
  parameter OPCODE_WIDTH  =  6;
  parameter OPERAND_WIDTH = 32;
  
  // port declaration
  input wire [OPCODE_WIDTH-1 :0] opcode  ;
  input wire [OPERAND_WIDTH-1:0] operand1;
  input wire [OPERAND_WIDTH-1:0] operand2;

  output wire [OPERAND_WIDTH-1:0] result ;

  // internal declarations
  reg [OPERAND_WIDTH-1:0] resultReg;

  // Assignment statements
  assign result = resultReg;


  //alu code
  always@( * ) begin
    case( opcode ) 
      ADD : resultReg = operand1 +  operand2;
      SUB : resultReg = operand1 +  operand2;
      AND : resultReg = operand1 &  operand2;
      OR  : resultReg = operand1 |  operand2;
      XOR : resultReg = operand1 ^  operand2;
      XNOR: resultReg = operand1 ~^ operand2;
      default : resultReg = 0;
    endcase  
  end
endmodule 

`endif
