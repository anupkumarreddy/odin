
`ifndef FETCH_COMPILE
`define FETCH_COMPILE

module Fetch( clk         , //!< Clock input to the fetch unit
              resetn      , //!< Reset for the fetch unit
              instruction , //!< Next instruction ready for decode
              loadPc      , //!< Load a new PC value in case of jumps
              nextAddress); //!< New PC value if the load Pc is one

  // Parameter declarations
  parameter INST_WIDTH = 32;

  // Port Declarations
  input  wire clk   ;
  input  wire resetn;
  input  wire loadPc;
  input  wire [INST_WIDTH-1:0] nextAddress;
  output wire [INST_WIDTH-1:0] instruction;

  // Assignment statements
  assign instruction = instructionReg;

  // Internal declarations
  reg [INST_WIDTH-1:0] instructionReg;

  // Fetching unit
  always@( posedge clk or negedge resetn ) begin
    if( ~resetn ) begin
      in
    end else begin
    end
  end 

endmodule   
`endif
