`ifndef INSTRUCTION_MEMORY_COMPILED
`define INSTRUCTION_MEMORY_COMPILED

module Memory( clk           , //!< Clk input to the memory
               resetn        , //!< Reset input to the memory, active low
               readEnable    , //!< Enables read port of the memory
               readAddress   , //!< Carries read address for read port
               readData     ); //!< Carries read data for read port 

  // Parameters for Generic memory
  parameter MEM_AWIDTH = 32  ;
  parameter MEM_DWIDTH = 32  ;
  parameter MEM_DEPTH  = 1024;


  // Port declarations for the memory
  input  wire                  readEnable   ;
  input  wire [MEM_AWIDTH-1:0] readAddress  ;
  output wire [MEM_DWIDTH-1:0] readData     ;

  // Two dimentional array for the Memory
  reg [MEM_DWIDTH-1:0] memory [MEM_DEPTH];

  // Temporary signal and reg declarations
  reg [MEM_DWIDTH-1:0] readDataReg;


  // Continous Assignments
  assign readData = readDataReg; 


  // Read port for the memory
  always@( posedge clk or negedge resetn ) begin
     if( ~resetn ) begin
        readDataReg <= 'b0;
     end else begin
       if( readEnable ) begin
          readDataReg <= memory[readAddress];  
       end else begin
          readDataReg <= 'b0;
       end
     end
  end


endmodule 

`endif
